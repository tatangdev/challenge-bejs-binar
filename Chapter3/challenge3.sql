-- CREATE DATABASE

CREATE DATABASE sekolah;

-- CREATE TABLES

CREATE TABLE guru (
    id BIGSERIAL PRIMARY KEY,
    id_kelas int DEFAULT NULL,
    nama varchar(50) NOT NULL
);

CREATE TABLE mata_pelajaran (
    id BIGSERIAL PRIMARY KEY,
    id_guru int NOT NULL,
    nama varchar(50) NOT NULL
);

CREATE TABLE kelas (
    id BIGSERIAL PRIMARY KEY,
    id_mata_pelajaran int NOT NULL
);

CREATE TABLE siswa (
    id BIGSERIAL PRIMARY KEY,
    id_kelas int NOT NULL,
    nama varchar(50)
);

-- DROP DATABASE

DROP DATABASE sekolah;

--  DROP TABLE

DROP TABLE guru;

-- RENAME DATABASE

ALTER DATABASE sekolah
RENAME TO sekolah_luar_biasa;

-- RENAME TABLE

ALTER TABLE siswa
RENAME TO murid;

-- ADD COLUMN

ALTER TABLE siswa
ADD COLUMN nomor_hp int NOT NULL

-- INSERT DATA TO TABLES

INSERT INTO guru (id_kelas, nama) VALUES (1, 'Bapak Udin');
INSERT INTO guru (id_kelas, nama) VALUES (2, 'Bapak Galang');
INSERT INTO guru (id_kelas, nama) VALUES (NULL, 'Bapak Yanto');
INSERT INTO guru (id_kelas, nama) VALUES (NULL, 'Ibu Sukmadi');
INSERT INTO guru (id_kelas, nama) VALUES (3, 'Ibu Budi');
INSERT INTO guru (id_kelas, nama) VALUES (4, 'Ibu Andi');

INSERT INTO mata_pelajaran (id_guru, nama) VALUES (1, 'BASIC MATH');
INSERT INTO mata_pelajaran (id_guru, nama) VALUES (2, 'INTERMEDIATE MATH');
INSERT INTO mata_pelajaran (id_guru, nama) VALUES (3, 'PRO MATH');
INSERT INTO mata_pelajaran (id_guru, nama) VALUES (4, 'ANOTHER LEVEL OF MATH');
INSERT INTO mata_pelajaran (id_guru, nama) VALUES (5, 'COUNSELING');

INSERT INTO kelas (id_mata_pelajaran) VALUES (1);
INSERT INTO kelas (id_mata_pelajaran) VALUES (2);
INSERT INTO kelas (id_mata_pelajaran) VALUES (3);
INSERT INTO kelas (id_mata_pelajaran) VALUES (4);

INSERT INTO siswa (id_kelas, nama) VALUES (1, 'Tono');
INSERT INTO siswa (id_kelas, nama) VALUES (1, 'Budi');
INSERT INTO siswa (id_kelas, nama) VALUES (1, 'Yuda');
INSERT INTO siswa (id_kelas, nama) VALUES (1, 'Nayeon');
INSERT INTO siswa (id_kelas, nama) VALUES (1, 'Karina');
INSERT INTO siswa (id_kelas, nama) VALUES (2, 'Wendy');
INSERT INTO siswa (id_kelas, nama) VALUES (2, 'Joy');
INSERT INTO siswa (id_kelas, nama) VALUES (2, 'Lisa');
INSERT INTO siswa (id_kelas, nama) VALUES (2, 'Sakura');
INSERT INTO siswa (id_kelas, nama) VALUES (2, 'Yuna');
INSERT INTO siswa (id_kelas, nama) VALUES (3, 'Ryujin');
INSERT INTO siswa (id_kelas, nama) VALUES (3, 'Eunha');
INSERT INTO siswa (id_kelas, nama) VALUES (3, 'Somi');
INSERT INTO siswa (id_kelas, nama) VALUES (3, 'Tyuzu');
INSERT INTO siswa (id_kelas, nama) VALUES (3, 'Sana');
INSERT INTO siswa (id_kelas, nama) VALUES (4, 'Momo');
INSERT INTO siswa (id_kelas, nama) VALUES (4, 'Yuju');
INSERT INTO siswa (id_kelas, nama) VALUES (4, 'Wonyoung');
INSERT INTO siswa (id_kelas, nama) VALUES (4, 'Bomi');
INSERT INTO siswa (id_kelas, nama) VALUES (4, 'Sukuna');

-- UPDATE TABLE

UPDATE guru
SET
    id_kelas = NULL
WHERE 
    guru.id_kelas = 4;

UPDATE guru
SET
    id_kelas = 4
WHERE
    guru.id = 4;

-- SELECT FROM TABLE

SELECT * FROM guru;
SELECT * FROM siswa;
SELECT * FROM mata_pelajaran;
SELECT * FROM kelas;

-- JOIN

SELECT
    guru.nama AS nama_guru,
    mata_pelajaran.nama AS pelajaran,
    kelas.id AS kelas,
    siswa.nama AS murid
FROM kelas
JOIN
    mata_pelajaran ON kelas.id_mata_pelajaran = mata_pelajaran.id
JOIN
    guru ON mata_pelajaran.id_guru = guru.id
JOIN
    siswa ON siswa.id_kelas = kelas.id;

-- JOIN SPECIFIC

SELECT
    guru.nama AS nama_guru,
    mata_pelajaran.nama AS pelajaran,
    kelas.id AS kelas,
    siswa.nama AS murid
FROM kelas
JOIN
    mata_pelajaran ON kelas.id_mata_pelajaran = mata_pelajaran.id
JOIN
    guru ON mata_pelajaran.id_guru = guru.id
JOIN
    siswa ON siswa.id_kelas = kelas.id
WHERE
    kelas.id = 1;

-- DELETE CLASS

DELETE FROM kelas WHERE kelas.id = 1;